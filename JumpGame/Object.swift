//
//  Object.swift
//  JumpGame
//
//  Created by Anton Nilsson on 2016-04-14.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

import Foundation
import SpriteKit

class Object : SKSpriteNode {
    
    var bodyType : UInt32 = 0
    
    init(imageName : String, objectBodyType: UInt32) {
        
        bodyType = objectBodyType
        let objectTexture = SKTexture(imageNamed: imageName)
        super.init(texture: objectTexture, color: UIColor.clearColor(), size: objectTexture.size())
        self.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.size.width, self.size.height*0.9))
        self.physicsBody!.dynamic = true
        self.physicsBody!.affectedByGravity = false
        
        if self.bodyType == BodyType.obstacle.rawValue {
            self.physicsBody!.contactTestBitMask = BodyType.projectile.rawValue
        } else {
            self.physicsBody!.contactTestBitMask = BodyType.obstacle.rawValue
        }
        self.physicsBody!.collisionBitMask = bodyType
        self.physicsBody!.categoryBitMask = bodyType
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder) has not been implemented")
    }
    
    func update(delta: NSTimeInterval) {
        self.position = CGPointMake(self.position.x - 1100*CGFloat(delta), self.position.y)
    }
}