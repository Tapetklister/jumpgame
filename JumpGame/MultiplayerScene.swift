//
//  MultiplayerScene.swift
//  JumpGame
//
//  Created by Anton Nilsson on 2016-04-27.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

import SpriteKit

class MultiplayerScene : SKScene {
    
    var runAnimation : [SKTexture] = [SKTexture]()
    var jumpAnimation : [SKTexture] = [SKTexture]()
    var shootAnimation : [SKTexture] = [SKTexture]()
    var deadAnimation : [SKTexture] = [SKTexture]()
    var runAtlas = SKTextureAtlas(named: "run")
    var jumpAtlas = SKTextureAtlas(named: "jump")
    var shootAtlas = SKTextureAtlas(named: "shoot")
    var deadAtlas = SKTextureAtlas(named: "dead")
    var p1Node : Character?
    var p2Node : Character?
    var projectileNode1 : Object?
    var projectileNode2 : Object?
    var groundPics : [SKSpriteNode] = []
    
    override func didMoveToView(view: SKView) {
        
        let fixer = Fixer()
        
        jumpAnimation = fixer.getSprites(jumpAtlas, imageString: "Jump", animation: jumpAnimation)
        shootAnimation = fixer.getSprites(shootAtlas, imageString: "Shoot", animation: shootAnimation)
        runAnimation = fixer.getSprites(runAtlas, imageString: "Run", animation: runAnimation)
        deadAnimation = fixer.getSprites(deadAtlas, imageString: "Dead", animation: deadAnimation)
        
        fixer.createGround("Desert", imageArray: groundPics, scene: self, speed: 0)
        
        p1Node = fixer.createCharacter(CGPointMake(frame.size.width*0.2, frame.size.height*0.25), runAnimation: runAnimation, jumpAnimation: jumpAnimation, shootAnimation: shootAnimation, deadAnimation: deadAnimation)
        p2Node = fixer.createCharacter(CGPointMake(frame.size.width*0.8, frame.size.height*0.25), runAnimation: runAnimation, jumpAnimation: jumpAnimation, shootAnimation: shootAnimation, deadAnimation: deadAnimation)
        
        self.addChild(p1Node!)
        self.addChild(p2Node!)
        
    }
    
}
