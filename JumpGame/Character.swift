//
//  Character.swift
//  BroGameExc
//
//  Created by Anton Nilsson on 2016-04-09.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

import Foundation
import SpriteKit

class Character : SKSpriteNode {
    
    var onGround = true
    var runAnimation : [SKTexture]
    var jumpAnimation : [SKTexture]
    var shootAnimation : [SKTexture]
    var deadAnimation : [SKTexture]
    var ableToShoot : Bool = true
    var dead : Bool = false;
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder) has not been implemented")
    }
    
    init (runTextures: [SKTexture], jumpTextures: [SKTexture], shootTextures: [SKTexture], deadTextures: [SKTexture]) {
        
        self.runAnimation = runTextures
        self.jumpAnimation = jumpTextures
        self.shootAnimation = shootTextures
        self.deadAnimation = deadTextures
        
        let textureSize = CGSizeMake(runAnimation[0].size().width/2, runAnimation[0].size().height/2)
        
        super.init(texture: runAnimation[0], color: UIColor.clearColor(), size: textureSize)
        
        let body : SKPhysicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(textureSize.width*0.4, textureSize.height*0.8))
        body.dynamic = true
        body.affectedByGravity = false
        body.allowsRotation = false
        body.categoryBitMask = BodyType.character.rawValue
        body.contactTestBitMask = BodyType.obstacle.rawValue
        body.collisionBitMask = BodyType.character.rawValue
        self.physicsBody = body
        
        animateLoop(self.runAnimation)
        
    }
    
    func jump() {
        
        if(onGround) {
            onGround = false
            self.animate(self.jumpAnimation)
            let up = SKAction.moveByX(0, y: 200, duration: 0.25)
            let wait = SKAction.waitForDuration(0.1)
            let down = SKAction.moveByX(0, y: -200, duration: 0.25)
            let jumpAction = SKAction.sequence([up, wait, down])
            self.runAction(SKAction .playSoundFileNamed("jump_01", waitForCompletion: false))
            self.runAction(jumpAction, completion: {
                self.onGround = true
            })
        }
    }
    
    func shoot() {
        
        if ableToShoot {
            ableToShoot = false
            let animation = SKAction.animateWithTextures(shootAnimation, timePerFrame: 0.08)
            self.runAction(SKAction .playSoundFileNamed("shot_01", waitForCompletion: false))
            self.runAction(animation, completion: {
                let wait = SKAction.waitForDuration(0.6)
                let enableShoot = SKAction.runBlock({ 
                    self.ableToShoot = true
                })
                let sequence = SKAction.sequence([wait, enableShoot])
                self.runAction(sequence)
            })
        }
    }
    
    func die() {
        dead = true;
        self.removeAllActions()
        animate(self.deadAnimation)
    }
    
    func animateLoop(animationTextures: [SKTexture]) {
        let animation = SKAction.animateWithTextures(animationTextures, timePerFrame: 0.06)
        let loop = SKAction.repeatActionForever(animation)
        self.runAction(loop)
    }
    
    func animate(animationTextures: [SKTexture]) {
        let animation = SKAction.animateWithTextures(animationTextures, timePerFrame: 0.06)
        self.runAction(animation)
    }

    
}