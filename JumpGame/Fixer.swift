//
//  Fixer.swift
//  JumpGame
//
//  Created by Anton Nilsson on 2016-04-27.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

import SpriteKit

class Fixer {
    
    func createGround(imageName: String, imageArray: [SKSpriteNode], scene: SKScene, speed: Double) -> [SKSpriteNode]{
        var vImageArray = imageArray
        let groundTexture = SKTexture(imageNamed: imageName)
        
        for index in 0...10 {
            vImageArray.append(SKSpriteNode(texture: groundTexture))
            vImageArray[index].position = CGPoint(x: (groundTexture.size().width/2 + (groundTexture.size().width * CGFloat(index))), y: groundTexture.size().height)
            vImageArray[index].zPosition = -5
            scene.addChild(vImageArray[index])
            
            vImageArray[index].physicsBody = SKPhysicsBody(rectangleOfSize: groundTexture.size())
            vImageArray[index].physicsBody!.dynamic = false
            
            moveLeft(speed, picture: vImageArray[index])
        }
        
        return vImageArray
    }
    
    func scrollBackground(imageName: String, imageArray: [SKSpriteNode], scene: SKScene, speed: Double) -> [SKSpriteNode] {
        var vImageArray = imageArray
        let bgTexture = SKTexture(imageNamed: imageName)
        vImageArray.append(SKSpriteNode(texture: bgTexture))
        vImageArray.append(SKSpriteNode(texture: bgTexture))
        
        
        for index in 0...1 {
            vImageArray[index] = SKSpriteNode(texture: bgTexture)
            vImageArray[index].zPosition = -30
            vImageArray[index].anchorPoint = CGPointZero
            vImageArray[index].position = CGPoint(x: (bgTexture.size().width * CGFloat(index)) - CGFloat(1 * index), y: 100)
            scene.addChild(vImageArray[index])
            moveLeft(10, picture: vImageArray[index])
        }
        return vImageArray
    }
    
    func moveLeft(speed: Double, picture: SKSpriteNode) {
    
        let moveLeft = SKAction.moveByX(-picture.texture!.size().width, y: 0, duration: speed)
        let moveReset = SKAction.moveByX(picture.texture!.size().width, y: 0, duration: 0)
        let moveLoop = SKAction.sequence([moveLeft, moveReset])
        let moveForever = SKAction.repeatActionForever(moveLoop)
        picture.runAction(moveForever)
        
    }
    
    func getSprites(atlas: SKTextureAtlas, imageString: String, animation: [SKTexture]) -> [SKTexture] {
        atlas.preloadWithCompletionHandler({})
        var newAnimation = animation
        for index in 1...atlas.textureNames.count {
            let textureName = String(format: "\(imageString)__%03d", index-1)
            newAnimation.append(atlas.textureNamed(textureName))
        }
        return newAnimation
    }
    
    func configureShootIndicator(node: SKShapeNode, scene: SKScene) {
        node.position = CGPointMake(scene.frame.size.width*0.1, scene.frame.size.height*0.8)
        node.fillColor = UIColor.greenColor()
        scene.addChild(node)
    }
    
    func createCharacter(position: CGPoint, runAnimation: [SKTexture], jumpAnimation: [SKTexture], shootAnimation: [SKTexture], deadAnimation: [SKTexture]) -> Character{
        
        let character = Character(runTextures: runAnimation, jumpTextures: jumpAnimation, shootTextures: shootAnimation, deadTextures: deadAnimation)
        character.position = position
        return character
    }
    
    func createScoreLabel(label: SKLabelNode, height: CGFloat, color: UIColor, scene: SKScene) -> SKLabelNode {
        var newLabel = label
        newLabel = SKLabelNode(fontNamed: "ScoreLabel")
        newLabel.fontSize = 80
        newLabel.fontColor = color
        newLabel.text = "0"
        newLabel.position = CGPointMake(scene.frame.size.width*0.9, height)
        return newLabel
    }
    
    func manageHighscore(score: Int, highscoreLabel: SKLabelNode) -> SKLabelNode {
        
        if NSUserDefaults().valueForKey("Highscore") != nil {
            let currentHighscore: Int = NSUserDefaults().valueForKey("Highscore") as! Int
            if score > currentHighscore {
                NSUserDefaults.standardUserDefaults().setValue(score, forKey: "Highscore")
                NSUserDefaults.standardUserDefaults().synchronize()
                highscoreLabel.text = "\(score)"
            }
        } else {
            NSUserDefaults.standardUserDefaults().setValue(score, forKey: "Highscore")
        }
        
        return highscoreLabel
    }


    
}
