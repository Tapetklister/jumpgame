//
//  GameScene.swift
//  JumpGame
//
//  Created by Anton Nilsson on 2016-04-13.
//  Copyright (c) 2016 Anton Nilsson. All rights reserved.
//

import SpriteKit

enum BodyType: UInt32 {
    case character = 1
    case ground = 2
    case obstacle = 4
    case projectile = 8
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var shootIndicator : SKShapeNode = SKShapeNode(circleOfRadius: 40)
    var runAnimation : [SKTexture] = [SKTexture]()
    var jumpAnimation : [SKTexture] = [SKTexture]()
    var shootAnimation : [SKTexture] = [SKTexture]()
    var deadAnimation : [SKTexture] = [SKTexture]()
    var runAtlas = SKTextureAtlas(named: "run")
    var jumpAtlas = SKTextureAtlas(named: "jump")
    var shootAtlas = SKTextureAtlas(named: "shoot")
    var deadAtlas = SKTextureAtlas(named: "dead")
    var groundLine = CGFloat(0)
    var characterNode : Character?
    var enemyNode : Object?
    var projectileNode : Object?
    var enemyDictionary = Dictionary<String, CGPoint>()
    var lastUpdateTime : NSTimeInterval = 0
    var onGroundPosition : CGFloat = CGFloat()
    var backgroundPics : [SKSpriteNode] = []
    var groundPics : [SKSpriteNode] = []
    var notDead : Bool = true
    var score : Int = 0;
    var scoreLabel = SKLabelNode()
    var highscoreLabel = SKLabelNode()
    let fixer : Fixer = Fixer()
    var backgroundMusic: SKAudioNode!
    let menuNode: SKLabelNode = SKLabelNode(text: "Menu")
    
    override func didMoveToView(view: SKView) {
        
        //view.showsPhysics = true
        physicsWorld.contactDelegate = self
        
        //addChild(SKAudioNode(fileNamed: "bgMusic"))
        
        groundPics = fixer.createGround("Desert", imageArray: groundPics, scene: self, speed: 0.1)
        onGroundPosition = groundPics[0].position.y + groundPics[0].size.height/2
        
        backgroundPics = fixer.scrollBackground("Background", imageArray: backgroundPics, scene: self, speed: 10)
        
        runAnimation = fixer.getSprites(runAtlas, imageString: "Run", animation: runAnimation)
        jumpAnimation = fixer.getSprites(jumpAtlas, imageString: "Jump", animation: jumpAnimation)
        shootAnimation = fixer.getSprites(shootAtlas, imageString: "Shoot", animation: shootAnimation)
        deadAnimation = fixer.getSprites(deadAtlas, imageString: "Dead", animation: deadAnimation)
        fixer.configureShootIndicator(shootIndicator, scene: self)
        characterNode = fixer.createCharacter(CGPointMake(200, 200), runAnimation: runAnimation, jumpAnimation: jumpAnimation, shootAnimation: shootAnimation, deadAnimation: deadAnimation)
        self.addChild(characterNode!)
        scoreLabel = fixer.createScoreLabel(scoreLabel, height: frame.size.height*0.8, color: UIColor.redColor(), scene: self)
        highscoreLabel = fixer.createScoreLabel(highscoreLabel, height: frame.size.height*0.7, color: UIColor.brownColor(), scene: self)
        addChild(scoreLabel)
        addChild(highscoreLabel)
        
        if let x = NSUserDefaults().valueForKey("Highscore") as? Int {
            highscoreLabel.text = String(x)
        } else {
            highscoreLabel.text = "-"
        }
        
        menuNode.position = CGPointMake(frame.size.width/2, frame.size.height*0.1+menuNode.fontSize/2)
        menuNode.fontSize = 50
        menuNode.fontColor = UIColor.yellowColor()
        menuNode.fontName = "AvenirNext-Bold"
        self.addChild(menuNode)
        
        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(randomize), userInfo: nil, repeats: true)
        
    }
    
    func randomize() {
        print("randomize")
        if notDead {
            if let n = self.enemyNode {
                removeChildrenInArray([n])
                score = score + 1
                scoreLabel.text = "\(score)"
                if score > NSUserDefaults().valueForKey("Highscore") as? Int {
                    self.highscoreLabel.text = String(score);
                    self.highscoreLabel.fontColor = UIColor.greenColor();
                }
            }
            self.enemyNode = nil
            let random = Int(arc4random_uniform(3))
            switch random {
                case 0: spawnObstacle("Enemy", height: onGroundPosition + frame.height/3)
                case 1: spawnObstacle("snakeLava__001", height: onGroundPosition)
                case 2: spawnObstacle("Crate", height: onGroundPosition)
                default: break
            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            if menuNode == self.nodeAtPoint(touch.locationInNode(self)) {
                let newScene = SKTransition.crossFadeWithDuration(0)
                let startScene = StartScene(fileNamed: "StartScene")
                startScene!.scaleMode = .AspectFill
                self.view?.presentScene(startScene!, transition: newScene)
            }
        }

        if !(characterNode?.dead)! {
            for touch in touches {
                if characterNode?.ableToShoot == true {
                    if touch.locationInNode(self).x > self.size.width/2 {
                        characterNode?.shoot()
                        if let n = self.projectileNode {
                            removeChildrenInArray([n])
                        }
                        spawnProjectile("Shot")
                    } else {
                        characterNode?.jump()
                    }

                }
            }
        } else {
            characterNode?.dead=false
            for pic in groundPics {
                pic.paused = false
            }
            for pic in backgroundPics {
                pic.paused = false
            }
            
            if let n = self.enemyNode {
                removeChildrenInArray([n])
            }
            characterNode?.animateLoop((characterNode?.runAnimation)!)
            characterNode?.ableToShoot=true
            characterNode?.onGround=true
            characterNode?.position = CGPointMake(200, 200)
            notDead = true
            score = 0
            scoreLabel.text = "\(score)"
            highscoreLabel.color = UIColor.brownColor()
            highscoreLabel.text = "\(NSUserDefaults().valueForKey("Highscore") as! Int)"
            highscoreLabel.fontColor = UIColor.brownColor()
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        
        characterNode?.position.x = 200
        let delta : NSTimeInterval = currentTime - self.lastUpdateTime
        self.lastUpdateTime = currentTime
        if notDead {
            enemyNode?.update(delta)
            
            if characterNode != nil{
                if characterNode!.ableToShoot == false {
                    shootIndicator.fillColor = UIColor.redColor()
                    projectileNode?.update(-delta)
                } else {
                    shootIndicator.fillColor = UIColor.greenColor()
                }
            }
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch(contactMask) {
        case BodyType.character.rawValue | BodyType.obstacle.rawValue:
            for index in 0...1 {
                backgroundPics[index].paused = true
            }
            for index in 0...10 {
                groundPics[index].paused = true
            }
            notDead = false
            characterNode?.die()
            fixer.manageHighscore(score, highscoreLabel: highscoreLabel)
            
        case BodyType.projectile.rawValue | BodyType.obstacle.rawValue:
            self.runAction(SKAction .playSoundFileNamed("hit1.mp3", waitForCompletion: false))
            if let n = self.projectileNode {
                removeChildrenInArray([n])
            }
            if let n = self.enemyNode {
                removeChildrenInArray([n])
            }
        default:
            return
        }
        
    }
    
    func spawnObstacle(name: String, height: CGFloat){
        let obstacle = Object(imageName: name, objectBodyType: BodyType.obstacle.rawValue)
        obstacle.position = CGPointMake(frame.width, height + obstacle.size.height/2)
        addChild(obstacle)
        
        enemyNode = obstacle
    }
    
    func spawnProjectile(name: String) {
        let projectile = Object(imageName: name, objectBodyType: BodyType.projectile.rawValue)
        projectile.position = CGPointMake((characterNode?.position.x)!, (characterNode?.position.y)!)
        addChild(projectile)
        projectileNode = projectile
    }
}
