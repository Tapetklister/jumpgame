//
//  StartScene.swift
//  JumpGame
//
//  Created by Anton Nilsson on 2016-04-27.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

import SpriteKit

class StartScene: SKScene {
    
    var titleLabel: SKLabelNode = SKLabelNode(text: "Endless Gunner")
    var button1 : SKSpriteNode = SKSpriteNode()
    var button1Text : SKLabelNode = SKLabelNode(text: "1P")
    var bgImage : SKSpriteNode = SKSpriteNode()
    var audioNode : SKAudioNode!
    
    override func didMoveToView(view: SKView) {
        
        titleLabel.fontColor = UIColor.brownColor()
        titleLabel.fontSize = 100
        titleLabel.position = CGPointMake(frame.size.width/2, frame.size.height*0.8-titleLabel.fontSize/2)
        titleLabel.fontName = "AvenirNext-Bold"
        self.addChild(titleLabel)
        
        button1 = SKSpriteNode (imageNamed: "Crate")
        button1.position = CGPointMake(frame.size.width/2, frame.size.height/2)
        button1.size = CGSizeMake(button1.size.width*2, button1.size.height*2)
        button1.zPosition = -15
        self.addChild(button1)
        button1Text.fontColor = UIColor.yellowColor()
        button1Text.position = button1.position
        button1Text.fontSize = 50
        button1Text.fontName = "AvenirNext-Bold"
        self.addChild(button1Text)
        
        bgImage = SKSpriteNode (imageNamed: "Background")
        bgImage.position = CGPointMake(frame.size.width/2, (frame.size.height*0.6))
        bgImage.zPosition = -30
        self.addChild(bgImage)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            if button1 == self.nodeAtPoint(touch.locationInNode(self)) {
                let newScene = SKTransition.crossFadeWithDuration(0)
                let gameScene = GameScene(fileNamed: "GameScene")
                gameScene!.scaleMode = .AspectFill
                self.view?.presentScene(gameScene!, transition: newScene)
            }
        }
        
    }

}
